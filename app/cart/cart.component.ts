import { module,
    IComponentOptions} from 'angular';
import { ModuleName } from '../app';
import { IProduct } from '../';
import {ServiceName as productServiceName, IProductService } from '../services/product.service';
import {CartServiceName as cartServiceName, ICartService} from '../services/cart.service';
import {ComponentName as bmProductTable} from '../product-table/product-table.component';

let componentName = 'bmCart';

class CartComponentController {

    static $inject = [
        productServiceName, cartServiceName
    ];

    products: IProduct[];

    constructor(private productService: IProductService, private cartService: ICartService) { 
        
    }

    $routerOnActivate() {
        this.cartService.getProducts().then(products => this.products = products);
    }
   
}

let cartComponent: IComponentOptions = {
    templateUrl: 'app/cart/cart.component.html',
    controller: CartComponentController,
    controllerAs: 'cart'
};

module(ModuleName).component(componentName, cartComponent);

export const ComponentName = componentName;