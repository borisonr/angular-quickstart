"use strict";
var angular_1 = require('angular');
var app_1 = require('../app');
var product_service_1 = require('../services/product.service');
var cart_service_1 = require('../services/cart.service');
var componentName = 'bmCart';
var CartComponentController = (function () {
    function CartComponentController(productService, cartService) {
        this.productService = productService;
        this.cartService = cartService;
    }
    CartComponentController.prototype.$routerOnActivate = function () {
        var _this = this;
        this.cartService.getProducts().then(function (products) { return _this.products = products; });
    };
    CartComponentController.$inject = [
        product_service_1.ServiceName, cart_service_1.CartServiceName
    ];
    return CartComponentController;
}());
var cartComponent = {
    templateUrl: 'app/cart/cart.component.html',
    controller: CartComponentController,
    controllerAs: 'cart'
};
angular_1.module(app_1.ModuleName).component(componentName, cartComponent);
exports.ComponentName = componentName;
//# sourceMappingURL=cart.component.js.map