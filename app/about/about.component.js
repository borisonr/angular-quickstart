"use strict";
var angular_1 = require('angular');
var app_1 = require('../app');
var componentName = 'bmAbout';
var AboutComponentController = (function () {
    function AboutComponentController() {
    }
    AboutComponentController.prototype.$routerOnActivate = function (next) { };
    return AboutComponentController;
}());
var aboutComponent = {
    templateUrl: 'app/about/about.component.html',
    controller: AboutComponentController
};
angular_1.module(app_1.ModuleName).component(componentName, aboutComponent);
exports.ComponentName = componentName;
//# sourceMappingURL=about.component.js.map