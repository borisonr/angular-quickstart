import { module,
    IComponentOptions } from 'angular';
import { ModuleName } from '../app';
import {ComponentName as productListComponentName} from '../product-list/product-list.component';
import {ComponentName as productComponentName} from '../product/product.component';
import {ComponentName as aboutComponentName} from '../about/about.component';
import {ComponentName as adminComponentName} from '../admin/admin.component';
import {ComponentName as cartComponentName} from '../cart/cart.component';
import {ComponentName as editProductComponentName} from '../edit-product/edit-product.component';
import {CartComponentName as cartInfo} from '../cart-info/cart-info.component';
// import '../product-table/product-table.component';

let componentName = 'bmAppRouter';

let appRouterComponent: IComponentOptions = {
    templateUrl: 'app/app-router/app-router.component.html',
    $routeConfig: [
        { path: '/products', name: 'Products', component: productListComponentName, useAsDefault: true },
        { path: '/product/:id', name: 'Product', component: productComponentName },
        { path: '/about', name: 'About', component: aboutComponentName },
        { path: '/admin', name: 'Admin', component: adminComponentName },
        { path: '/cart', name: 'Cart', component: cartComponentName },
        { path: '/edit-product/:id', name: 'EditProduct', component: editProductComponentName}
    ]
};

module(ModuleName).component(componentName, appRouterComponent);

export const ComponentName = componentName;



