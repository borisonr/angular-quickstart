"use strict";
var angular_1 = require('angular');
var app_1 = require('../app');
var product_list_component_1 = require('../product-list/product-list.component');
var product_component_1 = require('../product/product.component');
var about_component_1 = require('../about/about.component');
var admin_component_1 = require('../admin/admin.component');
var cart_component_1 = require('../cart/cart.component');
var edit_product_component_1 = require('../edit-product/edit-product.component');
// import '../product-table/product-table.component';
var componentName = 'bmAppRouter';
var appRouterComponent = {
    templateUrl: 'app/app-router/app-router.component.html',
    $routeConfig: [
        { path: '/products', name: 'Products', component: product_list_component_1.ComponentName, useAsDefault: true },
        { path: '/product/:id', name: 'Product', component: product_component_1.ComponentName },
        { path: '/about', name: 'About', component: about_component_1.ComponentName },
        { path: '/admin', name: 'Admin', component: admin_component_1.ComponentName },
        { path: '/cart', name: 'Cart', component: cart_component_1.ComponentName },
        { path: '/edit-product/:id', name: 'EditProduct', component: edit_product_component_1.ComponentName }
    ]
};
angular_1.module(app_1.ModuleName).component(componentName, appRouterComponent);
exports.ComponentName = componentName;
//# sourceMappingURL=app-router.component.js.map