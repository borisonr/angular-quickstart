import { module,
    IComponentOptions, IScope } from 'angular';
import { ModuleName } from '../app';
import { IProduct } from '../models';

let componentName = 'bmProductTable';

class ProductTableComponentController {
    // isAdmin: boolean = false;

    //  products: IProduct[];

    // $routerOnActivate(next) {
    //     this.products = next.params.products;
    // }
    // console.log(products)
}

let productTableComponent: IComponentOptions = {
    templateUrl: 'app/product-table/product-table.component.html',
    controller: ProductTableComponentController,
    controllerAs: 'productTable',
    bindings: {
        products: '<',
        isAdmin: '<'
  }
};

module(ModuleName).component(componentName, productTableComponent);

export const ComponentName = componentName;