"use strict";
var angular_1 = require('angular');
var app_1 = require('../app');
var componentName = 'bmProductTable';
var ProductTableComponentController = (function () {
    function ProductTableComponentController() {
    }
    return ProductTableComponentController;
}());
var productTableComponent = {
    templateUrl: 'app/product-table/product-table.component.html',
    controller: ProductTableComponentController,
    controllerAs: 'productTable',
    bindings: {
        products: '<',
        isAdmin: '<'
    }
};
angular_1.module(app_1.ModuleName).component(componentName, productTableComponent);
exports.ComponentName = componentName;
//# sourceMappingURL=product-table.component.js.map