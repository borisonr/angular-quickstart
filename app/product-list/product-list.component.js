"use strict";
var angular_1 = require('angular');
var app_1 = require('../app');
var product_service_1 = require('../services/product.service');
var componentName = 'bmProductList';
var ProductListComponentController = (function () {
    function ProductListComponentController(productService) {
        this.productService = productService;
    }
    ProductListComponentController.prototype.$routerOnActivate = function () {
        var _this = this;
        this.productService.getProducts().then(function (value) { return _this.products = value; });
    };
    ProductListComponentController.$inject = [
        product_service_1.ServiceName
    ];
    return ProductListComponentController;
}());
var productListComponent = {
    templateUrl: 'app/product-list/product-list.component.html',
    controller: ProductListComponentController,
    controllerAs: 'productList'
};
angular_1.module(app_1.ModuleName).component(componentName, productListComponent);
exports.ComponentName = componentName;
//# sourceMappingURL=product-list.component.js.map