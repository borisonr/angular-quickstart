"use strict";
var angular_1 = require('angular');
require('@angular/router/angular1/angular_1_router');
var appName = 'app';
exports.ModuleName = appName;
angular_1.module(appName, ['ngComponentRouter'])
    .config(function ($locationProvider) {
    $locationProvider.html5Mode(true);
})
    .value('$routerRootComponent', 'bmAppRouter');
angular_1.element(document).ready(function () { return angular_1.bootstrap(document, [appName]); });
require('./app-router/app-router.component');
require('./index');
//# sourceMappingURL=app.js.map