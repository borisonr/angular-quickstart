"use strict";
var angular_1 = require('angular');
var app_1 = require('../app');
var product_service_1 = require('../services/product.service');
var cart_service_1 = require('../services/cart.service');
var componentName = 'bmProduct';
var ProductComponentController = (function () {
    function ProductComponentController(productService, cartService, $rootScope) {
        this.productService = productService;
        this.cartService = cartService;
        this.$rootScope = $rootScope;
    }
    ProductComponentController.prototype.addProduct = function (product) {
        this.cartService.addProduct(product);
        this.$rootScope.$emit('newProduct');
        //implement go to cart page
    };
    ProductComponentController.prototype.$routerOnActivate = function (next) {
        var _this = this;
        this.id = next.params.id;
        this.productService.getProducts().then(function (results) {
            for (var _i = 0, results_1 = results; _i < results_1.length; _i++) {
                var product = results_1[_i];
                if (product.id === next.params.id)
                    _this.myProduct = product;
            }
        });
    };
    ProductComponentController.$inject = [
        product_service_1.ServiceName, cart_service_1.CartServiceName, '$rootScope'
    ];
    return ProductComponentController;
}());
var productComponent = {
    templateUrl: 'app/product/product.component.html',
    controller: ProductComponentController,
    controllerAs: 'product'
};
angular_1.module(app_1.ModuleName).component(componentName, productComponent);
exports.ComponentName = componentName;
//# sourceMappingURL=product.component.js.map