import { module,
    IComponentOptions, IScope, IRootScopeService } from 'angular';
import { ModuleName } from '../app';
import { IProduct } from '../models';
import {ServiceName as productServiceName, IProductService } from '../services/product.service';
import {CartServiceName as cartServiceName, ICartService} from '../services/cart.service';

let componentName = 'bmProduct';

class ProductComponentController {

     static $inject = [
        productServiceName, cartServiceName, '$rootScope'
    ];

    private id: number;
    private myProduct: IProduct;

 

    constructor(private productService: IProductService, private cartService: ICartService, private $rootScope: IRootScopeService) {}

    public addProduct(product: IProduct): void {
        this.cartService.addProduct(product);
        this.$rootScope.$emit('newProduct')
        //implement go to cart page
        
    }

    $routerOnActivate(next) {
        this.id = next.params.id;
        this.productService.getProducts().then(results => {
            for(let product of results){
                if(product.id === next.params.id) this.myProduct = product;
            }
        })
    }
}

let productComponent: IComponentOptions = {
    templateUrl: 'app/product/product.component.html',
    controller: ProductComponentController,
    controllerAs: 'product'
};

module(ModuleName).component(componentName, productComponent);

export const ComponentName = componentName;