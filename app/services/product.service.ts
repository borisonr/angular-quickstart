import { module,
    IQService,
    IPromise } from 'angular';
import { ModuleName } from '../app';
import { IProduct } from '../models';
import {ServiceName as messagingServiceName, IMessagingService } from '../services/messaging.service';

let serviceName = 'productService';

export interface IProductService {
    getProducts(): IPromise<IProduct[]>;
    deleteProduct(productToDelete: IProduct): IPromise<IProduct[]>;
    addProduct(product: IProduct): IPromise<IProduct[]>;
    editProduct(newProduct: IProduct): IPromise<IProduct[]>;
}

export class ProductService implements IProductService {

    static $inject = [
        '$q', messagingServiceName
    ];
    products: IProduct[] = [
            { id: 1, name: 'Cake', price: 9.99, info: 'Tastes delicious', alert: false },
            { id: 2, name: 'Ice Cream', price: 7.99, info: 'Eat it before it melts', alert: false },
        ];

    constructor(private $q: IQService, private messagingService: IMessagingService) { }

    getProducts(): IPromise<IProduct[]> {
        return this.$q.when(this.products);
    }
    deleteProduct(productToDelete) {
        this.products.forEach(product => {
            if(product.id === productToDelete.id) this.products.splice(this.products.indexOf(product), 1)
        })
        this.messagingService.emit(productToDelete);
        return this.$q.when(this.products);
    }
    addProduct(product) {
        this.products.push(product);
        return this.$q.when(this.products);
        }
    editProduct(newProduct) {
        this.products.forEach(product => {
            if(product.id === newProduct.id) this.products.splice(this.products.indexOf(product), 1)
        })
        this.products.push(newProduct);
        return this.$q.when(this.products);
    }
}

module(ModuleName).service(serviceName, ProductService);

export const ServiceName = serviceName;

