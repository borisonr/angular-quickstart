"use strict";
var angular_1 = require('angular');
var app_1 = require('../app');
var messaging_service_1 = require('../services/messaging.service');
var serviceName = 'productService';
var ProductService = (function () {
    function ProductService($q, messagingService) {
        this.$q = $q;
        this.messagingService = messagingService;
        this.products = [
            { id: 1, name: 'Cake', price: 9.99, info: 'Tastes delicious', alert: false },
            { id: 2, name: 'Ice Cream', price: 7.99, info: 'Eat it before it melts', alert: false },
        ];
    }
    ProductService.prototype.getProducts = function () {
        return this.$q.when(this.products);
    };
    ProductService.prototype.deleteProduct = function (productToDelete) {
        var _this = this;
        this.products.forEach(function (product) {
            if (product.id === productToDelete.id)
                _this.products.splice(_this.products.indexOf(product), 1);
        });
        this.messagingService.emit(productToDelete);
        return this.$q.when(this.products);
    };
    ProductService.prototype.addProduct = function (product) {
        this.products.push(product);
        return this.$q.when(this.products);
    };
    ProductService.prototype.editProduct = function (newProduct) {
        var _this = this;
        this.products.forEach(function (product) {
            if (product.id === newProduct.id)
                _this.products.splice(_this.products.indexOf(product), 1);
        });
        this.products.push(newProduct);
        return this.$q.when(this.products);
    };
    ProductService.$inject = [
        '$q', messaging_service_1.ServiceName
    ];
    return ProductService;
}());
exports.ProductService = ProductService;
angular_1.module(app_1.ModuleName).service(serviceName, ProductService);
exports.ServiceName = serviceName;
//# sourceMappingURL=product.service.js.map