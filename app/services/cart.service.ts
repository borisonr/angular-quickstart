import { module,
    IQService,
    IPromise } from 'angular';
import { ModuleName } from '../app';
import { IProduct } from '../models';
import {ServiceName as productServiceName, IProductService } from './product.service';

let serviceName = 'CartService';

export interface ICartService {
    getProducts(): IPromise<IProduct[]> ;
    addProduct(product: IProduct): IPromise<IProduct[]>;
}

class CartService implements ICartService {

    static $inject = [
        '$q', productServiceName
    ];

    products: IProduct[];

    constructor(private $q: IQService,  private productService: IProductService) { }

    getProducts(): IPromise<IProduct[]> {
        //check for price discrepancy and alert user if there is a new price
        this.productService.getProducts().then(adminProducts=>{
            if(this.products){
            this.products.forEach(product=>{
                adminProducts.forEach(adminProduct=>{
                    if(adminProduct.id === product.id && adminProduct.price != product.price){
                        product.price = adminProduct.price;
                        product.alert = true;
                    }
                })
            })
            }
        })
        return this.$q.when(this.products);
    }
    addProduct(product: IProduct) {
        if(!this.products) this.products = [];
        let myProduct = angular.copy(product);
        myProduct.alert = false;
        this.products.push(myProduct);
        return this.$q.when(this.products);
    }
}

module(ModuleName).service(serviceName, CartService);

export const CartServiceName = serviceName;