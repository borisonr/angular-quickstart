"use strict";
var angular_1 = require('angular');
var app_1 = require('../app');
var product_service_1 = require('./product.service');
var serviceName = 'CartService';
var CartService = (function () {
    function CartService($q, productService) {
        this.$q = $q;
        this.productService = productService;
    }
    CartService.prototype.getProducts = function () {
        var _this = this;
        //check for price discrepancy and alert user if there is a new price
        this.productService.getProducts().then(function (adminProducts) {
            if (_this.products) {
                _this.products.forEach(function (product) {
                    adminProducts.forEach(function (adminProduct) {
                        if (adminProduct.id === product.id && adminProduct.price != product.price) {
                            product.price = adminProduct.price;
                            product.alert = true;
                        }
                    });
                });
            }
        });
        return this.$q.when(this.products);
    };
    CartService.prototype.addProduct = function (product) {
        if (!this.products)
            this.products = [];
        var myProduct = angular.copy(product);
        myProduct.alert = false;
        this.products.push(myProduct);
        return this.$q.when(this.products);
    };
    CartService.$inject = [
        '$q', product_service_1.ServiceName
    ];
    return CartService;
}());
angular_1.module(app_1.ModuleName).service(serviceName, CartService);
exports.CartServiceName = serviceName;
//# sourceMappingURL=cart.service.js.map