import { module,
    IQService,
    IPromise, IRootScopeService } from 'angular';
import { ModuleName } from '../app';
import { IProduct } from '../models';

let serviceName = 'messagingService';

export interface IMessagingService {
    emit(product: IProduct) 
}

export class MessagingService implements IMessagingService {

    static $inject = [
        '$q', '$rootScope'
    ];

    constructor(private $q: IQService, private $rootScope: IRootScopeService) { }

    emit(product) {
        this.$rootScope.$emit('deleteProduct', {product});
    }

}

module(ModuleName).service(serviceName, MessagingService);

export const ServiceName = serviceName;

