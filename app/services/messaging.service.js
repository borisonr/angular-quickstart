"use strict";
var angular_1 = require('angular');
var app_1 = require('../app');
var serviceName = 'messagingService';
var MessagingService = (function () {
    function MessagingService($q, $rootScope) {
        this.$q = $q;
        this.$rootScope = $rootScope;
    }
    MessagingService.prototype.emit = function (product) {
        this.$rootScope.$emit('deleteProduct', { product: product });
    };
    MessagingService.$inject = [
        '$q', '$rootScope'
    ];
    return MessagingService;
}());
exports.MessagingService = MessagingService;
angular_1.module(app_1.ModuleName).service(serviceName, MessagingService);
exports.ServiceName = serviceName;
//# sourceMappingURL=messaging.service.js.map