import { module,
    IComponentOptions, IRootScopeService } from 'angular';
import { ModuleName } from '../app';
import { IProduct } from '../';
import {ServiceName as productServiceName, IProductService } from '../services/product.service';
import {CartServiceName as cartServiceName, ICartService} from '../services/cart.service';
import {ComponentName as bmProductTable} from '../product-table/product-table.component';

let componentName = 'bmCartInfo';

class CartInfoComponentController {

    static $inject = [
        productServiceName, cartServiceName, '$rootScope'
    ];

    products: IProduct[];
    message: string;

    constructor(private productService: IProductService, private cartService: ICartService, private $rootScope: IRootScopeService) { 
        this.$rootScope.$on('deleteProduct', (event, product) => {
            product = product.product;

            if(this.products.length > 0 ) {
                
                this.products.forEach(cartProduct=>{
                    if(cartProduct.id === product.id) {
                        this.products.splice(this.products.indexOf(cartProduct), 1);
                        this.message = product.name + " is no longer available on our site";
                    }
                })
            }
        })
        this.$rootScope.$on('newProduct', event => this.cartService.getProducts().then(products => this.products = products))
    }

    $routerOnActivate() {
        this.cartService.getProducts().then(products => this.products = products);
    }
   
}

let cartInfoComponent: IComponentOptions = {
    templateUrl: 'app/cart-info/cart-info.component.html',
    controller: CartInfoComponentController,
    controllerAs: 'cartInfo'
};

module(ModuleName).component(componentName, cartInfoComponent);

export const CartComponentName = componentName;