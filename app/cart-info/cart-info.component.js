"use strict";
var angular_1 = require('angular');
var app_1 = require('../app');
var product_service_1 = require('../services/product.service');
var cart_service_1 = require('../services/cart.service');
var componentName = 'bmCartInfo';
var CartInfoComponentController = (function () {
    function CartInfoComponentController(productService, cartService, $rootScope) {
        var _this = this;
        this.productService = productService;
        this.cartService = cartService;
        this.$rootScope = $rootScope;
        this.$rootScope.$on('deleteProduct', function (event, product) {
            product = product.product;
            if (_this.products.length > 0) {
                _this.products.forEach(function (cartProduct) {
                    if (cartProduct.id === product.id) {
                        _this.products.splice(_this.products.indexOf(cartProduct), 1);
                        _this.message = product.name + " is no longer available on our site";
                    }
                });
            }
        });
        this.$rootScope.$on('newProduct', function (event) { return _this.cartService.getProducts().then(function (products) { return _this.products = products; }); });
    }
    CartInfoComponentController.prototype.$routerOnActivate = function () {
        var _this = this;
        this.cartService.getProducts().then(function (products) { return _this.products = products; });
    };
    CartInfoComponentController.$inject = [
        product_service_1.ServiceName, cart_service_1.CartServiceName, '$rootScope'
    ];
    return CartInfoComponentController;
}());
var cartInfoComponent = {
    templateUrl: 'app/cart-info/cart-info.component.html',
    controller: CartInfoComponentController,
    controllerAs: 'cartInfo'
};
angular_1.module(app_1.ModuleName).component(componentName, cartInfoComponent);
exports.CartComponentName = componentName;
//# sourceMappingURL=cart-info.component.js.map