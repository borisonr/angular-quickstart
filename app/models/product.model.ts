export interface IProduct {
    id: number;
    name: string;
    price: number;
    info: string;
    alert: boolean;
}