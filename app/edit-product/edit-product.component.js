"use strict";
var angular_1 = require('angular');
var app_1 = require('../app');
var product_service_1 = require('../services/product.service');
var componentName = 'bmEditProduct';
var EditProductComponentController = (function () {
    function EditProductComponentController(productService) {
        this.productService = productService;
    }
    EditProductComponentController.prototype.deleteProduct = function (product) {
        this.productService.deleteProduct(product);
        //go to home
    };
    EditProductComponentController.prototype.editProduct = function (product, newProduct) {
        console.log(newProduct);
        newProduct.id = product.id;
        if (!newProduct.name)
            newProduct.name = product.name;
        if (!newProduct.price)
            newProduct.price = product.price;
        if (!newProduct.info)
            newProduct.info = product.info;
        this.productService.editProduct(newProduct);
        //go to home or product page
    };
    EditProductComponentController.prototype.$routerOnActivate = function (next) {
        var _this = this;
        this.id = next.params.id;
        this.productService.getProducts().then(function (results) {
            for (var _i = 0, results_1 = results; _i < results_1.length; _i++) {
                var product = results_1[_i];
                if (product.id === _this.id) {
                    _this.myProduct = product;
                }
            }
        });
    };
    EditProductComponentController.$inject = [
        product_service_1.ServiceName
    ];
    return EditProductComponentController;
}());
exports.EditProductComponentController = EditProductComponentController;
var editProductComponent = {
    templateUrl: 'app/edit-product/edit-product.component.html',
    controller: EditProductComponentController,
    controllerAs: 'editProduct'
};
angular_1.module(app_1.ModuleName).component(componentName, editProductComponent);
exports.ComponentName = componentName;
//# sourceMappingURL=edit-product.component.js.map