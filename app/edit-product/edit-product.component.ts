import { module,
    IComponentOptions, IScope } from 'angular';
import { ModuleName } from '../app';
import { IProduct } from '../models';
import {ServiceName as productServiceName, IProductService } from '../services/product.service';

let componentName = 'bmEditProduct';

export class EditProductComponentController {

     static $inject = [
        productServiceName
    ];

    private id: number;
    private myProduct: IProduct;

    constructor(private productService: IProductService) {}

    deleteProduct(product: IProduct) {
        this.productService.deleteProduct(product)
        //go to home
    }

    editProduct(product: IProduct, newProduct: IProduct) {
        console.log(newProduct);
        newProduct.id = product.id;
        if(!newProduct.name) newProduct.name = product.name;
        if(!newProduct.price) newProduct.price = product.price;
        if(!newProduct.info) newProduct.info = product.info;
        this.productService.editProduct(newProduct)
        //go to home or product page
    }

    $routerOnActivate(next) {
        this.id = next.params.id;
        this.productService.getProducts().then(results => {
            for(let product of results){
                if(product.id === this.id) {
                    this.myProduct = product;
                }
            }
        })
        
    }
}

let editProductComponent: IComponentOptions = {
    templateUrl: 'app/edit-product/edit-product.component.html',
    controller: EditProductComponentController,
    controllerAs: 'editProduct'
};

module(ModuleName).component(componentName, editProductComponent);

export const ComponentName = componentName;