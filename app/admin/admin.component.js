"use strict";
var angular_1 = require('angular');
var app_1 = require('../app');
var product_service_1 = require('../services/product.service');
var componentName = 'bmAdmin';
var AdminComponentController = (function () {
    function AdminComponentController(productService) {
        this.productService = productService;
        this.isAdmin = true;
    }
    AdminComponentController.prototype.addProduct = function (product) {
        this.productService.addProduct(product);
    };
    AdminComponentController.prototype.$routerOnActivate = function () {
        var _this = this;
        this.productService.getProducts().then(function (value) { return _this.products = value; });
    };
    AdminComponentController.$inject = [
        product_service_1.ServiceName
    ];
    return AdminComponentController;
}());
var adminComponent = {
    templateUrl: 'app/admin/admin.component.html',
    controller: AdminComponentController,
    controllerAs: 'admin'
};
angular_1.module(app_1.ModuleName).component(componentName, adminComponent);
exports.ComponentName = componentName;
//# sourceMappingURL=admin.component.js.map