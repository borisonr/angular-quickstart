import { module,
    IComponentOptions } from 'angular';
import { ModuleName } from '../app';
import { IProduct } from '../';
import {ServiceName as productServiceName, IProductService } from '../services/product.service';
import {ComponentName as bmProductTable} from '../product-table/product-table.component';

let componentName = 'bmAdmin';

class AdminComponentController {

    static $inject = [
        productServiceName
    ];

    products: IProduct[];
    isAdmin = true;

    constructor(private productService: IProductService) { }

    addProduct(product: IProduct) {
        this.productService.addProduct(product)
    }



    $routerOnActivate() {
        this.productService.getProducts().then(value => this.products = value);
    }
}

let adminComponent: IComponentOptions = {
    templateUrl: 'app/admin/admin.component.html',
    controller: AdminComponentController,
    controllerAs: 'admin'
};

module(ModuleName).component(componentName, adminComponent);

export const ComponentName = componentName;